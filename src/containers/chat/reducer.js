import {
    SET_ALL_MESSAGES,
    SEND_MESSAGE,
    DELETE_MESSAGE,
    UPDATE_MESSAGE,
    TOGGLE_REACTION,
    SET_EDITED_MESSAGE,
} from './actionTypes';

const initialState = {
    user: {
        id: '246f0ee8-cb13-11ea-87d0-0242ac130003',
        avatar: 'https://resizing.flixster.com/IaXbRF4gIPh9jireK_4VCPNfdKc=/280x250/v2/https://flxt.tmsimg.com/v2/AllPhotos/150692/150692_v2_bb.jpg',
        name: 'Maksym'
    },
    chatName: 'Miners from Kyiv',
    usersNumber: 3,
    messages: undefined,
    messageToEdit: undefined,
    lastActionType:''
}

export default (state = initialState, action) => {
    switch (action.type) {
        case SET_ALL_MESSAGES:
            return {
                ...state,
                lastActionType:action.type,
                messages: [...action.messages]
            };
        case SEND_MESSAGE:
            return {
                ...state,
                lastActionType:action.type,
                messages: [...state.messages, action.message]
            };
        case DELETE_MESSAGE:
            return {
                ...state,
                lastActionType:action.type,
                messages: [...state.messages.filter(m => m.id !== action.idToDelete)]
            };
        case UPDATE_MESSAGE:
            return {
                ...state,
                messages: [...state.messages.map(m => m.id === action.message.id ? action.message : m)],
                lastActionType:action.type,
                messageToEdit: undefined
            };
        case TOGGLE_REACTION:
            let messageToReactOn = {...state.messages.find(m => m.id === action.messageId)};
            messageToReactOn.isLiked = !messageToReactOn.isLiked;
            return {
                ...state,
                lastActionType:action.type,
                messages: [...state.messages.map(m => m.id === action.messageId ? messageToReactOn : m)]
            };
        case SET_EDITED_MESSAGE:
            return {
                ...state,
                lastActionType:action.type,
                messageToEdit: action.message
            };
        default:
            return state;
    }
};
