import React from 'react';
import {Loader} from 'semantic-ui-react'
import {connect} from 'react-redux';
import Header from "../../components/Header";
import MessageList from "../../components/MessageList";
import ChatInfo from "../../components/ChatInfo";
import MessageInput from "../../components/MessageInput";
import MessageEditor from "../../components/MessageEditor";
import {bindActionCreators} from "redux";
import '../../styles/common.css'

import {
    cancelUpdatingMessage,
    deleteMessage,
    loadMessages,
    sendMessage,
    setEditedMessage,
    toggleReaction,
    updateMessage,
    setLastMessageAsEdited
} from "./actions";


class Chat extends React.Component {

    componentDidMount() {
        this.props.loadMessages();
    }

    render() {
        return (
            <div className={"content-wrp"}>
                <Header/>
                {this.props.messages && <div className={"content-wrp, grid-wrp"}>
                    <div className={"info-wrp"}>
                        <ChatInfo
                            chatName={this.props.chatName}
                            messages={this.props.messages}
                            participantsNumber={this.props.usersNumber}/>
                    </div>
                    <div className={"list-wrp"}>
                        <MessageList
                            messages={this.props.messages}
                            reactionToggle={this.props.toggleReaction}
                            deleteMessage={this.props.deleteMessage}
                            setMessageToEdit={this.props.setEditedMessage}
                            editMessage={this.props.editMessage}
                            lastAction={this.props.lastAction}
                            user={this.props.user}/>
                    </div>
                    <div className={"input-wrp"}>
                        <MessageInput
                            send={this.props.sendMessage}
                            user={this.props.user}
                            updateLastMessage={this.props.setLastMessageAsEdited}
                        />
                    </div>
                </div>}
                {!this.props.messages && <div className={"loader"}>
                    <Loader active size='large' inline='centered'>Connecting to the nearest mine...</Loader>
                </div>
                }
                {this.props.messageToEdit && <MessageEditor
                    edit={this.props.updateMessage}
                    cancel={this.props.cancelUpdatingMessage}
                    message={this.props.messageToEdit}
                />}
            </div>
        );
    }
};

const mapStateToProps = rootState => {
    return ({
        user: rootState.chat.user,
        chatName: rootState.chat.chatName,
        usersNumber: rootState.chat.usersNumber,
        messages: rootState.chat.messages,
        messageToEdit: rootState.chat.messageToEdit,
        lastAction: rootState.chat.lastActionType
    });
}

const actions = {
    loadMessages,
    setEditedMessage,
    setLastMessageAsEdited,
    deleteMessage,
    updateMessage,
    cancelUpdatingMessage,
    sendMessage,
    toggleReaction
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Chat);
