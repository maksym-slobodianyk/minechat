import {
    DELETE_MESSAGE,
    SEND_MESSAGE,
    SET_ALL_MESSAGES,
    SET_EDITED_MESSAGE,
    TOGGLE_REACTION,
    UPDATE_MESSAGE,
} from './actionTypes';
import axios from "axios";


const setMessagesAction = messages => ({
    type: SET_ALL_MESSAGES,
    messages
});

const sendMessageAction = message => ({
    type: SEND_MESSAGE,
    message
});

const deleteMessageAction = idToDelete => ({
    type: DELETE_MESSAGE,
    idToDelete
});
const updateMessageAction = message => ({
    type: UPDATE_MESSAGE,
    message
});
const toggleReactionAction = messageId => ({
    type: TOGGLE_REACTION,
    messageId
});

const setEditedMessageAction = message => ({
    type: SET_EDITED_MESSAGE,
    message
});

export const loadMessages = () => async dispatch => {
    await new Promise(r => setTimeout(r, 2000));
    axios.get('https://edikdolynskyi.github.io/react_sources/messages.json')
        .then(response => {
            const messages = response.data.sort((a, b) => (a.createdAt > b.createdAt) ? 1 : -1);
            dispatch(setMessagesAction(messages))
        });
};

export const sendMessage = message => dispatch => {
    dispatch(sendMessageAction(message));
};

export const updateMessage = message => dispatch => {
    dispatch(updateMessageAction(message));
};

export const deleteMessage = idToDelete => dispatch => {
    dispatch(deleteMessageAction(idToDelete));
};

export const toggleReaction = messageId => dispatch => {
    dispatch(toggleReactionAction(messageId));
};

export const setEditedMessage = message => dispatch => {
    dispatch(setEditedMessageAction(message));
};

export const cancelUpdatingMessage = () => dispatch => {
    dispatch(setEditedMessageAction(undefined));
}

export const setLastMessageAsEdited = () => (dispatch, getRootState) => {
    const {chat: {messages, user}} = getRootState();
    console.log(  messages.filter(m => m.userId === user.id));
    console.log(  messages.filter(m => m.userId === user.id).sort((a, b) => a.createdAt < b.createdAt));
    dispatch(setEditedMessageAction(messages.filter(m => m.userId === user.id).sort((a, b) => (a.createdAt > b.createdAt) ? 1 : -1)[0]));
}
