import React from 'react';
import {Header as SemanticHeader} from 'semantic-ui-react';
import './header.css'
import '../../styles/common.css'

const Header = () => (
    <div className={"header-container"}>
        <SemanticHeader className={"logo"} as='h2' image='https://image.flaticon.com/icons/svg/2767/2767716.svg' content='MineChat'/>
    </div>
);
export default Header;
