import React from 'react';
import Message from "../Message";
import PropTypes from "prop-types";
import './message-list.css'
import '../../styles/common.css'

class MessageList extends React.Component {

    previousDate = new Date(0);

    monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];


    constructor(props) {
        super(props);
        let scrollRef = new React.createRef();
    }

    componentDidMount() {
        this.scrollRef.scrollIntoView({behavior: "auto"});
        this.previousDate = new Date(0)
    }

    componentDidUpdate() {
        if (this.props.lastAction === 'MESSAGE_ACTION:SEND_MESSAGE') {
            this.scrollRef.scrollIntoView({behavior: "smooth"});
        }
        this.previousDate = new Date(0);
    }

    equalDates(previous, messageDate) {
        if (previous.getFullYear() !== messageDate.getFullYear()) {
            return false;
        } else if (previous.getMonth() !== messageDate.getMonth()) {
            return false;
        } else if (previous.getDate() !== messageDate.getDate()) {
            return false;
        }
        return true;
    }

    generateStamp() {
        let stamp = '';
        if (this.equalDates(this.previousDate, new Date())) {
            return "Today";
        } else if (this.equalDates(this.previousDate, new Date(Date.now() - 24 * 3600 * 1000))) {
            return "Yesterday";
        } else if (this.previousDate.getFullYear() !== (new Date()).getFullYear()) {
            return stamp.concat(this.previousDate.getDate(), ' of ', this.monthNames[this.previousDate.getMonth()], '  ', this.previousDate.getFullYear())
        } else {
            return stamp.concat(this.previousDate.getDate(), ' of ', this.monthNames[this.previousDate.getMonth()])
        }
    }

    render() {
        return (

            <div className={"container, msg-list"}>
                <div className={"message"}>
                    <ul>
                        {this.props.messages.map(message => {
                            if (!this.equalDates(this.previousDate, new Date(message.createdAt))) {
                                this.previousDate = new Date(message.createdAt);
                                return (
                                    <div key={message.id}>
                                        <li className="msg-day"
                                        ><small>{this.generateStamp()}</small></li>
                                        <Message
                                            message={message}
                                            user={this.props.user}
                                            reactionToggle={this.props.reactionToggle}
                                            setMessageToEdit={this.props.setMessageToEdit}
                                            deleteMessage={this.props.deleteMessage}
                                        />
                                    </div>
                                )
                            } else {
                                return (
                                    <Message
                                        key={message.id}
                                        message={message}
                                        user={this.props.user}
                                        reactionToggle={this.props.reactionToggle}
                                        setMessageToEdit={this.props.setMessageToEdit}
                                        deleteMessage={this.props.deleteMessage}
                                    />
                                )
                            }

                        })
                        }
                        <li style={{float: "left", clear: "both"}}
                            ref={(el) => {
                                this.scrollRef = el;
                            }}>
                        </li>
                    </ul>
                </div>
            </div>
        )
    }
}

MessageList.propTypes = {
    messages: PropTypes.array.isRequired,
    user: PropTypes.object.isRequired,
    lastAction: PropTypes.string.isRequired,
    reactionToggle: PropTypes.func.isRequired,
    setMessageToEdit: PropTypes.func.isRequired,
    deleteMessage: PropTypes.func.isRequired

};

export default MessageList;
