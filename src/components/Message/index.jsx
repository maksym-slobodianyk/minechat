import React, {useState} from 'react';
import {Icon, Image} from 'semantic-ui-react'
import PropTypes from "prop-types";
import '../MessageList/message-list.css'
import '../../styles/common.css'

const Message = ({
                     message,
                     user,
                     reactionToggle,
                     deleteMessage,
                     setMessageToEdit
                 }) => {

    const currentUserMessage = user.id === message.userId;
    const lastMessageTime = new Date(message.createdAt)
    const [isShown, setIsShown] = useState(false);

    return (
        <li className={currentUserMessage ? "msg-right" : "msg-left"}>
            <div className={"msg-container"}
                 onMouseEnter={() => setIsShown(true)}
                 onMouseLeave={() => setIsShown(false)}>
                {!currentUserMessage && <Image size="small" src={message.avatar}/>}
                <div className="msg-desc">
                    <p className={"msg-text"}>{message.text}</p>
                    <div className={"meta-container"}>
                        {message.editedAt && <small>edited</small>}
                        <small>{lastMessageTime.getHours() < 10 ? '0'.concat(lastMessageTime.getHours()) : lastMessageTime.getHours()}:
                            {lastMessageTime.getMinutes() < 10 ? '0'.concat(lastMessageTime.getMinutes()) : lastMessageTime.getMinutes()}</small>
                    </div>
                </div>
                {!currentUserMessage && <Icon className={"like"}
                                              name='diamond'
                                              color={message.isLiked ? "blue" : ""}
                                              onClick={() => reactionToggle(message.id)}/>}


                {currentUserMessage && isShown &&
                <small className={"delete-btn"} onClick={() => deleteMessage(message.id)}>delete</small>}

                {currentUserMessage && isShown &&
                <small className={"edit-btn"} onClick={() => setMessageToEdit(message)}>edit</small>}
            </div>
        </li>
    );
}

Message.propTypes = {
    message: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
    reactionToggle: PropTypes.func.isRequired,
    setMessageToEdit: PropTypes.func.isRequired,
    deleteMessage: PropTypes.func.isRequired,
};

export default Message;
