import React from 'react';
import PropTypes from "prop-types";
import './chat-info.css'
import '../../styles/common.css'

const ChatInfo = ({chatName, participantsNumber, messages}) => {
    const lastMessageTime =new Date(messages.map(m => m.createdAt).sort()[messages.length-1]);
    return (
        <div className={"container, grid-info-wrp"}>
            <div className={"chat-name"}>
                {chatName}
            </div>
            <div className={"participants-num"}>
                {participantsNumber} participants
            </div>
            <div className={"messages-num"}>
                {messages.length} messages
            </div>
            <div className={"last-message"}>
                last message at:  {lastMessageTime.getHours() < 10 ? '0'.concat(lastMessageTime.getHours()) : lastMessageTime.getHours()}:
                {lastMessageTime.getMinutes() < 10 ? '0'.concat(lastMessageTime.getMinutes()) : lastMessageTime.getMinutes()}
            </div>
        </div>
    );
}

ChatInfo.propTypes = {
    messages: PropTypes.array.isRequired,
    chatName: PropTypes.string.isRequired,
    participantsNumber: PropTypes.number.isRequired,
};


export default ChatInfo;
