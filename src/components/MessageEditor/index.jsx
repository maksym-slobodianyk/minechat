import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Modal, Segment } from 'semantic-ui-react';

const MessageEditor = ({edit, cancel, message}) => {
  const [editedText, setEditedText] = useState(message.text);
  const [edited, setEdited] = useState(false);

  const handleEditMessage = () => {
    if (editedText.length && editedText.trim()) {
      edit({
        ...message,
        "text": editedText,
        "editedAt": new Date()
      });
      setEditedText('');
    }
  }

  const handleType = (newText) => {
    setEditedText(newText);
    if (newText.length === 0 || newText.trim().length===0) {
      setEdited(false)
    } else if (newText === message.text) {
      setEdited(false)
    } else {
      setEdited(true)
    }
  }

  return (
    <Modal dimmer="blurring" centered open onClose={() => cancel()}>
      <Segment>
        <Form className={"container"} onSubmit={handleEditMessage}>
          <Form.Group className={"form-wrp"}>
            <Form.TextArea
                className={"field"}
                placeholder='Write edited text...'
                name='message'
                value={editedText}
                onChange={ev => handleType(ev.target.value)}
            />
            <Form.Button circular color={"olive"} className={"top-btn"} disabled={!edited} icon={"send"}/>
            <Form.Button circular color={"olive"} className={"bottom-btn"} icon={"cancel"} onClick={cancel}/>
          </Form.Group>
        </Form>
      </Segment>
    </Modal>
  );
};

MessageEditor.propTypes = {
  edit: PropTypes.func.isRequired,
  cancel: PropTypes.func.isRequired,
  message: PropTypes.objectOf(PropTypes.any).isRequired
};


export default MessageEditor;
